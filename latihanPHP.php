<!DOCTYPE html>
<html>
    <head>
        <title>Latihan 1 PHP</title>
    </head>
    <body>
        <h1>Belajar Menjalankan kode php dengan xampp</h1>
        <table>
            <tr>
                <td><?php echo "halo" ?></td>
            </tr>
        </table>
        <?php
        //echo adalah fungsi untuk menampilkan data bukan array
        echo "Belajar menampilkan text dari kode php";

        /**
         * memasukan html ke php
         */
        echo "<br>";
        echo "<h2>list makanan</h2>";
        echo "<ol>";
        echo "<li>nasi</li>";
        echo "<li>ikan</li>";
        echo "</ol>";

        //menggunakan variable php
        $sayuran = "bayam, kakung";

        echo "ini nama sayuran " . $sayuran . "<br>";

        //ini untuk type data

        $number = 1234567890;
        $string = "ini text haloo";
        $boleanTrue = true;
        $boleanFalse = false;
        $arrayPertama = ["nasi", "jagung"];
        $arrayKedua = array("nasi", "jagung");

        echo "ini type number " . $number . "<br>";
        echo "ini type string " . $string . "<br>";
        echo "ini type bolean true " . $boleanTrue . "<br>";
        echo "ini type bolean false " . $boleanFalse . "<br>";
        echo "ini type array " . var_dump($arrayPertama) . "<br>";

        //ini loping/perulangan dengan php
        foreach ($arrayPertama as $key => $value) {
            echo $value . "<br>";
        }
        ?>
    </body>
</html>